<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CinemaController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ForgortController;
use App\Http\Controllers\RatingController;
use App\Http\Controllers\LikeController;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});



// public routes
Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);






// protected routes
Route::group(['middleware' => ['auth:sanctum']], function (){
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/cinema', [CinemaController::class, 'store']);
    Route::put('/cinema/{id}', [CinemaController::class, 'update']);
    Route::delete('/cinema/{id}', [CinemaController::class, 'destroy']);
    Route::get('/cinema', [CinemaController::class, 'index']);
Route::get('/cinema/{id}', [CinemaController::class, 'show']);
Route::get('/SingleCinemaRatings/{id}', [CinemaController::class, 'SingleCinemaRatings']);
Route::get('/AllCinemaRatings', [CinemaController::class, 'AllCinemaRatings']);
Route::get('/cinema/search/{name}', [CinemaController::class, 'search']);
Route::post('/forgot',[ForgortController::class, 'forgot']);
Route::post('/reset',[ForgortController::class, 'reset']);
Route::get('/rating', [RatingController::class, 'index']);
Route::get('/rating/{id}', [RatingController::class, 'show']);
Route::post('/rating', [RatingController::class, 'store']);
Route::post('/like', [LikeController::class, 'store']);
    
});




// if you want all the basic routes this is an option
// Route::resource('cinema', CinemaController::class);









// return Cinema::create([
//     'Cinema_name' => 'Imperial',
//     'Cinema_info' => 'This is a geate cinema',
//     'Cinema_picture' => 'Imperial.jpg',
//     'Cinema_opening_hours' => '10-22',
//     'Cinema_address' => 'some address',
//     'Cinema_city' => 'Copenhagen',
//     'Cinema_status' => 1,
// ]);