<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'rating_id',
        'like_status'
    ];




      //a like belongs to a user
      public function user()
      {
          return $this-belongsTo(User::class);
      }
  
       //a like belongs to a rating
       public function cinema()
       {
           return $this-belongsTo(Rating::class, "table_foreign_key", 'rating_id');
       }
}
