<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\Like;


class Rating extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'cinema_id',
        'rating_comment',
        'rating_comfort',
        'rating_price',
        'rating_snacks',
        'rating_status'
    ];



    //a rating belongs to a user
    public function user()
    {
        return $this-belongsTo(User::class);
    }

     //a rating belongs to a cinema
     public function cinema()
     {
         return $this-belongsTo(Cinema::class);
     }

     public function Likes()
     {
         return $this->hasMany(Like::class, 'rating_id');
     }
}
