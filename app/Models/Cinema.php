<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cinema extends Model
{
    use HasFactory;
    protected $fillable = [
        'Cinema_name',
        'Cinema_info',
        'Cinema_picture',
        'Cinema_opening_hours',
        'Cinema_address',
        'Cinema_city',
        'Cinema_status'
    ];

    public function ratings()
    {
        return $this-hasMany(Rating::class);
    }
}