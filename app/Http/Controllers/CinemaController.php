<?php

namespace App\Http\Controllers;

use App\Models\Cinema;
use Illuminate\Http\Request;
use App\Models\Rating;

class CinemaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Cinema::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'Cinema_name' => 'required',
            'Cinema_info' => 'required',
            'Cinema_picture' => 'required',
            'Cinema_opening_hours' => 'required',
            'Cinema_address' => 'required',
            'Cinema_city' => 'required',
            'Cinema_status' => 'required',
        ]);
        return Cinema::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Cinema::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Cinema = Cinema::find($id);
        $Cinema->update($request->all());
        return $Cinema;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Cinema::destroy($id);
    }



 /**
     * Search fro a name
     *
     * @param  str  $name
     * @return \Illuminate\Http\Response
     */
    public function search($name)
    {
        return Cinema::where('Cinema_name', 'like', '%'.$name.'%')->get();
    }

    public function SingleCinemaRatings($id)
    {
        $value = Rating::with('likes')->join('cinemas', 'ratings.cinema_id', '=', 'cinemas.id')
        ->join('users', 'users.id', '=', 'ratings.user_id')
        ->where('cinemas.id', $id)
        ->select(['ratings.id','ratings.user_id','ratings.cinema_id','ratings.created_at','cinemas.Cinema_name','users.name','ratings.rating_comfort','ratings.rating_snacks','ratings.rating_price','ratings.rating_comment'])
        ->get();

        $data = [];
        $avg = $value->avg('rating_price');
        $avg2 = $value->avg('rating_snacks');
        $avg3 = $value->avg('rating_comfort');
         array_push($data , $value);
         array_push($data , $avg);
         array_push($data , $avg2);
         array_push($data , $avg3);
        return $data;





        // $value = Cinema::join('ratings', 'cinemas.id', '=', 'ratings.cinema_id')
        // ->join('users', 'users.id', '=', 'ratings.user_id')->with('like')
        // ->where('cinemas.id', $id)
        // ->select(['ratings.id','ratings.user_id','ratings.cinema_id','ratings.created_at','cinemas.Cinema_name','users.name','ratings.rating_comfort','ratings.rating_snacks','ratings.rating_price','ratings.rating_comment'])
        // ->get();

        // $data = [];
        // $avg = $value->avg('rating_price');
        // $avg2 = $value->avg('rating_snacks');
        // $avg3 = $value->avg('rating_comfort');
        //  array_push($data , $value);
        //  array_push($data , $avg);
        //  array_push($data , $avg2);
        //  array_push($data , $avg3);
        // return $data;
    }

    public function AllCinemaRatings()
    {
        //cinema1
        $cinema1ratings = Cinema::join('ratings', 'cinemas.id', '=', 'ratings.cinema_id')
        ->where('cinemas.id', 1)
        ->select(['ratings.rating_comfort','ratings.rating_snacks','ratings.rating_price'])
        ->get();
        $cinema1 = Cinema::find(1);

        $cinema1->{"avg_price"} = $cinema1ratings->avg('rating_price');
        $cinema1->{"avg_snack"} = $cinema1ratings->avg('rating_snacks');
        $cinema1->{"avg_comfort"} = $cinema1ratings->avg('rating_comfort');
    

        //cinema2
        $cinema2ratings = Cinema::join('ratings', 'cinemas.id', '=', 'ratings.cinema_id')
        ->where('cinemas.id', 2)
        ->select(['ratings.rating_comfort','ratings.rating_snacks','ratings.rating_price'])
        ->get();
        $cinema2 = Cinema::find(2);

        $cinema2->{"avg_price"} = $cinema2ratings->avg('rating_price');
        $cinema2->{"avg_snack"} = $cinema2ratings->avg('rating_snacks');
        $cinema2->{"avg_comfort"} = $cinema2ratings->avg('rating_comfort');


         //cinema3
         $cinema3ratings = Cinema::join('ratings', 'cinemas.id', '=', 'ratings.cinema_id')
         ->where('cinemas.id', 3)
         ->select(['ratings.rating_comfort','ratings.rating_snacks','ratings.rating_price'])
         ->get();
         $cinema3 = Cinema::find(3);

        $cinema3->{"avg_price"} = $cinema3ratings->avg('rating_price');
        $cinema3->{"avg_snack"} = $cinema3ratings->avg('rating_snacks');
        $cinema3->{"avg_comfort"} = $cinema3ratings->avg('rating_comfort');

         //cinema4
         $cinema4ratings = Cinema::join('ratings', 'cinemas.id', '=', 'ratings.cinema_id')
         ->where('cinemas.id', 4)
         ->select(['ratings.rating_comfort','ratings.rating_snacks','ratings.rating_price'])
         ->get();
         $cinema4 = Cinema::find(4);

        $cinema4->{"avg_price"} = $cinema4ratings->avg('rating_price');
        $cinema4->{"avg_snack"} = $cinema4ratings->avg('rating_snacks');
        $cinema4->{"avg_comfort"} = $cinema4ratings->avg('rating_comfort');


          //cinema5
         $cinema5ratings = Cinema::join('ratings', 'cinemas.id', '=', 'ratings.cinema_id')
         ->where('cinemas.id', 5)
         ->select(['ratings.rating_comfort','ratings.rating_snacks','ratings.rating_price'])
         ->get();
         $cinema5 = Cinema::find(5);

        $cinema5->{"avg_price"} = $cinema5ratings->avg('rating_price');
        $cinema5->{"avg_snack"} = $cinema5ratings->avg('rating_snacks');
        $cinema5->{"avg_comfort"} = $cinema5ratings->avg('rating_comfort');

        //cinema6
         $cinema6ratings = Cinema::join('ratings', 'cinemas.id', '=', 'ratings.cinema_id')
         ->where('cinemas.id', 6)
         ->select(['ratings.rating_comfort','ratings.rating_snacks','ratings.rating_price'])
         ->get();
         $cinema6 = Cinema::find(6);

        $cinema6->{"avg_price"} = $cinema6ratings->avg('rating_price');
        $cinema6->{"avg_snack"} = $cinema6ratings->avg('rating_snacks');
        $cinema6->{"avg_comfort"} = $cinema6ratings->avg('rating_comfort');

        //cinema7
         $cinema7ratings = Cinema::join('ratings', 'cinemas.id', '=', 'ratings.cinema_id')
         ->where('cinemas.id', 7)
         ->select(['ratings.rating_comfort','ratings.rating_snacks','ratings.rating_price'])
         ->get();
         $cinema7 = Cinema::find(7);

        $cinema7->{"avg_price"} = $cinema7ratings->avg('rating_price');
        $cinema7->{"avg_snack"} = $cinema7ratings->avg('rating_snacks');
        $cinema7->{"avg_comfort"} = $cinema7ratings->avg('rating_comfort');

        //cinema8
         $cinema8ratings = Cinema::join('ratings', 'cinemas.id', '=', 'ratings.cinema_id')
         ->where('cinemas.id', 8)
         ->select(['ratings.rating_comfort','ratings.rating_snacks','ratings.rating_price'])
         ->get();
         $cinema8 = Cinema::find(8);

        $cinema8->{"avg_price"} = $cinema8ratings->avg('rating_price');
        $cinema8->{"avg_snack"} = $cinema8ratings->avg('rating_snacks');
        $cinema8->{"avg_comfort"} = $cinema8ratings->avg('rating_comfort');


        //cinema9
         $cinema9ratings = Cinema::join('ratings', 'cinemas.id', '=', 'ratings.cinema_id')
         ->where('cinemas.id', 9)
         ->select(['ratings.rating_comfort','ratings.rating_snacks','ratings.rating_price'])
         ->get();
         $cinema9 = Cinema::find(9);

        $cinema9->{"avg_price"} = $cinema9ratings->avg('rating_price');
        $cinema9->{"avg_snack"} = $cinema9ratings->avg('rating_snacks');
        $cinema9->{"avg_comfort"} = $cinema9ratings->avg('rating_comfort');


        //cinema10
         $cinema10ratings = Cinema::join('ratings', 'cinemas.id', '=', 'ratings.cinema_id')
         ->where('cinemas.id', 10)
         ->select(['ratings.rating_comfort','ratings.rating_snacks','ratings.rating_price'])
         ->get();
         $cinema10 = Cinema::find(10);

        $cinema10->{"avg_price"} = $cinema10ratings->avg('rating_price');
        $cinema10->{"avg_snack"} = $cinema10ratings->avg('rating_snacks');
        $cinema10->{"avg_comfort"} = $cinema10ratings->avg('rating_comfort');


        //cinema11
        $cinema11ratings = Cinema::join('ratings', 'cinemas.id', '=', 'ratings.cinema_id')
        ->where('cinemas.id', 11)
        ->select(['ratings.rating_comfort','ratings.rating_snacks','ratings.rating_price'])
        ->get();
        $cinema11 = Cinema::find(11);

       $cinema11->{"avg_price"} = $cinema11ratings->avg('rating_price');
       $cinema11->{"avg_snack"} = $cinema11ratings->avg('rating_snacks');
       $cinema11->{"avg_comfort"} = $cinema11ratings->avg('rating_comfort');


       //cinema12
       $cinema12ratings = Cinema::join('ratings', 'cinemas.id', '=', 'ratings.cinema_id')
       ->where('cinemas.id', 12)
       ->select(['ratings.rating_comfort','ratings.rating_snacks','ratings.rating_price'])
       ->get();
       $cinema12 = Cinema::find(12);

      $cinema12->{"avg_price"} = $cinema12ratings->avg('rating_price');
      $cinema12->{"avg_snack"} = $cinema12ratings->avg('rating_snacks');
      $cinema12->{"avg_comfort"} = $cinema12ratings->avg('rating_comfort');


      //cinema13
      $cinema13ratings = Cinema::join('ratings', 'cinemas.id', '=', 'ratings.cinema_id')
      ->where('cinemas.id', 13)
      ->select(['ratings.rating_comfort','ratings.rating_snacks','ratings.rating_price'])
      ->get();
      $cinema13 = Cinema::find(13);

     $cinema13->{"avg_price"} = $cinema13ratings->avg('rating_price');
     $cinema13->{"avg_snack"} = $cinema13ratings->avg('rating_snacks');
     $cinema13->{"avg_comfort"} = $cinema13ratings->avg('rating_comfort');


     //cinema14
     $cinema14ratings = Cinema::join('ratings', 'cinemas.id', '=', 'ratings.cinema_id')
     ->where('cinemas.id', 14)
     ->select(['ratings.rating_comfort','ratings.rating_snacks','ratings.rating_price'])
     ->get();
     $cinema14 = Cinema::find(14);

    $cinema14->{"avg_price"} =  $cinema14ratings->avg('rating_price');
    $cinema14->{"avg_snack"} = $cinema14ratings->avg('rating_snacks');
    $cinema14->{"avg_comfort"} = $cinema14ratings->avg('rating_comfort');


    //cinema15
    $cinema15ratings = Cinema::join('ratings', 'cinemas.id', '=', 'ratings.cinema_id')
    ->where('cinemas.id', 15)
    ->select(['ratings.rating_comfort','ratings.rating_snacks','ratings.rating_price'])
    ->get();
    $cinema15 = Cinema::find(15);

   $cinema15->{"avg_price"} =  $cinema15ratings->avg('rating_price');
   $cinema15->{"avg_snack"} =  $cinema15ratings->avg('rating_snacks');
   $cinema15->{"avg_comfort"} = $cinema15ratings->avg('rating_comfort');
    



    $maindata= [$cinema1,$cinema2,$cinema3,$cinema4,$cinema5,$cinema6,$cinema7,$cinema8,$cinema9,$cinema10,$cinema11,$cinema12,$cinema13,$cinema14,$cinema15];
    //sort
    if(request('sort')== 'price'){
        usort($maindata, function ($a, $b) { 
            if ($a->avg_price > $b->avg_price){
                return -1;
            }
            elseif ($a->avg_price < $b->avg_price){
                return 1;
            }
          });
    }else if(request('sort')== 'snack'){
    usort($maindata, function ($a, $b) { 
        if ($a->avg_snack > $b->avg_snack ){
            return -1;
        }
        elseif ($a->avg_snack < $b->avg_snack){
            return 1;
        }
        });
    }else if(request('sort')== 'comfort'){
    usort($maindata, function ($a, $b) {   if ($a->avg_comfort > $b->avg_comfort ){
        return -1;
    }
    elseif ($a->avg_comfort < $b->avg_comfort){
        return 1;
    }});
    }
    return $maindata;
    }
}
