<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Like;


class LikeController extends Controller
{
    public function store(Request $request)
    {
        if (Like::where('user_id', $request->user_id)->where('rating_id', $request->rating_id)->exists()) {
            $id =  Like::where('user_id', $request->user_id)->where('rating_id', $request->rating_id)->get();
         return Like::destroy($id);
        }
        $request->validate([
            'user_id' => 'required',
            'rating_id' => 'required',
            'like_status' => 'required',
        ]);
        return Like::create($request->all());
    }
}
