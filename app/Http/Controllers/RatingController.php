<?php

namespace App\Http\Controllers;

use App\Models\Rating;
use Illuminate\Http\Request;

class RatingController extends Controller
{
    public function index()
    {
        return Rating::where('user_id', 1)->get();
    }


    public function store(Request $request)
    {
        if (Rating::where('user_id', $request->user_id)->where('cinema_id', $request->cinema_id)->exists()) {
            return response([
                'message' => 'You have aldready rated this cinema'
            ], 401);
        }
        $request->validate([
            'user_id' => 'required',
            'cinema_id' => 'required',
            'rating_comfort' => 'required',
            'rating_price' => 'required',
            'rating_snacks' => 'required',
            'rating_status' => 'required',
        ]);
        return Rating::create($request->all());
    }




    public function show($id)
    {
        return Rating::where('cinema_id', $id)->get();
    }



    
}
