<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ForgortRequest;
use App\Http\Requests\ResetRequest;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class ForgortController extends Controller
{
    public function forgot(ForgortRequest $request){
        $email = $request->input(key: 'email');

        if(User::where('email', $email)->doesntExist()){
            return response([
                'message' => 'User dont exist'
            ], status: 404);
        }
        $token = Str::random(length:10);

        try{

        DB::table(table: 'password_resets')->insert([
            'email' => $email,
            'token' => $token
        ]);
   

        //send email

        return response([
            'message' => 'Here is the token supposed to be sent to email',
            'token' => $token
        ]);

        
    }catch(\Exception $exception){
        return response([
            'message' => $exception->getMessage()
        ], status: 400);
    }
    }
    public function reset(ResetRequest $request){
        $token = $request->input(key:'token');
        if(!$passwordResets = DB::table(table: 'password_resets')->where('token', $token)->first()){
            return response([
                'message' => 'Invalid token!'
            ], status: 400);
        }
        /** @var User $user */
        if(!$user = User::where('email', $passwordResets->email)->first()){
            return response([
                'message' => 'User doesnt exist!'
            ], status: 404);
        }
        $user->password = Hash::make(value: $request->input(key:'password'));
        $user->save();

        return response([
            'message' => 'Succes'
        ]);
    

    }
}
